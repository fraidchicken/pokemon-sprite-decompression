#include <stdio.h>
#include <stdlib.h>
#include "header.h"

void fgetRLE(char *buf, FILE *fp){
	char c;
	int count = 0, n, n1 = 0, n2 = 0;
	do{
		c = fgetc(fp);
		n = c == '1' ? 1 : 0;
		n1 = n1 * 2 + n;
		count++;
	}while(c != '0');

	int i;
	for(i = 0; i < count; i++){
		c = fgetc(fp);
		n = c == '1' ? 1 : 0;
		n2 = n2 * 2 + n;
	}

	int sum = n1 + n2 + 1;

	for(i = 0; i < sum * 2; i++) buf[i] = '0';
	buf[i] = '\0';
}

void fgetData(char *buf, FILE *fp){
	int i = 0, flag = 1;
	char c1, c2;

	while(flag){
		c1 = fgetc(fp);
		c2 = fgetc(fp);

		if(c1 == '0' && c2 == '0'){
			buf[i] = '\0';
			flag = 0;
		}else{
			buf[i] = c1;
			i++;
			buf[i] = c2;
			i++;
		}
	}
}

void fill_buffer(int *buffer_end, FILE *f, int size, char packet_type){
	int counter = 0;
	char buffer[1024];

	while(counter < size){
		switch(packet_type){
			case '0':
				fgetRLE(buffer, f);
				break;
			case '1':
				fgetData(buffer, f);
				break;
		}

		for(int i = 0; buffer[i] != '\0' && counter < size; counter++, i++){
			buffer_end[counter] = buffer[i] - 48;
		}

		printf("Counter: %d\tSize: %d\n", counter, size);
		printf("Packet_type: %d\tBuffer: %s\n", packet_type, buffer);

		packet_type = packet_type == '1' ? '0' : '1';
	}
}

void delta_decode(int *buffer, int len){
	int flag = 0;

	for(int i = 0; i < len; i++){
		flag = 0;
		for(int j = 0; j < len / 2; j++){
			for(int k = 0; k < 2; k++){ //sorry
				if(buffer[i * 2 + j * 2 * len + k] == 1 && flag == 0){
					flag = 1;
				}else if(buffer[i * 2 + j * 2 * len + k] == 1 && flag == 1){
					buffer[i * 2 + j * 2 * len + k] = 0;
					flag = 0;
				}else if(buffer[i * 2 + j * 2 * len + k] == 0 && flag == 1){
					buffer[i * 2 + j * 2 * len + k] = 1;
				}
			}
		}
	}
}


void file_buffer_2bpp(FILE *f, int *buffer, int len){
	for(int i = 0; i < len; i++){
		for(int j = 0; j < len / 2; j++){
			fprintf(f, "%d%d", buffer[i * 2 + j * len * 2], buffer[i * 2 + j * len * 2 + 1]);
		}
		fprintf(f, "\n");
	}
}

int main(){
	FILE *f = fopen("my_test.txt", "r");
	char buffer[8], start, primary;
	int size = 1;

	fgets(buffer, 5, f);	//width
	int width = binstr_int(buffer);
	size *= width * 8;
	int len = size;
	fgets(buffer, 5, f);	//height
	int height = binstr_int(buffer);
	size *= height * 8;

	printf("size: %d\n", size);

	int *bufferA = malloc(sizeof(int) * size);
	int *bufferB = malloc(sizeof(int) * size);

	primary = fgetc(f);		//primary buffer
	start = fgetc(f);	//0 - RLE, 1 - Data

	if(primary == '0'){
		int* temp = bufferA;
		bufferA = bufferB;
		bufferB = temp;
	}

	fill_buffer(bufferA, f, size, start);

	//encoding
	int encoding = fgetc(f) == '1' ? fgetc(f) - 47 : 0;

	start = fgetc(f);

	fill_buffer(bufferB, f, size, start);

	print_buffer_1bpp(bufferA, len);

	delta_decode(bufferA, len);

	switch(encoding){
		case 0:
			printf("Mode 1\n");
			delta_decode(bufferB, len);
			break;
		case 1:
			printf("Mode 2\n");
			xor(bufferB, bufferA, len);
			break;
		case 2:
			printf("Mode 3\n");
			delta_decode(bufferB, len);
			xor(bufferB, bufferA, len);
			break;
	}

	print_buffer_1bpp(bufferA, len);
	print_buffer_1bpp(bufferB, len);

	merge_buffer(bufferA, bufferB, len);
	print_buffer_2bpp(bufferA, len);

	// FILE *out = fopen("decompressed.txt", "w");
	// fprintf(out, "%d %d\n", width, height);
	// file_buffer_2bpp(out, bufferA, len);
	return 0;
}
