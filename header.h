#ifdef DEBUG

char p_fgetc(FILE *fp, int line){
		char c = fgetc(fp);
			printf("line: %d\t__fgetc__: |%c|\n", line, c);
				return c;
}

void p_fgets(char *buf, int n, FILE *fp, int line){
		fgets(buf, n, fp);
			printf("line: %d\t__fgets__: |%s|\n", line, buf);
}

#define fgetc(fp) p_fgetc(fp, __LINE__)
#define fgets(buf, n, fp) p_fgets(buf, n, fp, __LINE__)
#endif


int binstr_int(char *buf){
	int n = 0;
	for(int i = 0; buf[i] != '\0'; i++){
		n = n * 2 + (buf[i] == '1' ? 1 : 0);
	}
	return n;
}

void int_binstr(char *buffer, int n, int l){
	for(int i = 0; i < l; i++){
		buffer[l - i - 1] = n % 2 == 1 ? '1' : '0';
		n = n / 2;
	}
}

void print_buffer_1bpp(int *buffer, int len){
	char list_1bpp[] = " #";
	for(int i = 0; i < len; i++){
		for(int j = 0; j < len / 2; j++){
			printf("%c", list_1bpp[buffer[i * 2 + j * len * 2]]);
			printf("%c", list_1bpp[buffer[i * 2 + j * len * 2 + 1]]);
		}
		printf("\n");
	}
}

void print_buffer_2bpp(int *buffer, int len){
	char list_2bpp[] = " .*#";
	for(int i = 0; i < len; i++){
		for(int j = 0; j < len / 2; j++){
			printf("%c", list_2bpp[buffer[i * 2 + j * len * 2]]);
			printf("%c", list_2bpp[buffer[i * 2 + j * len * 2 + 1]]);
		}
		printf("\n");
	}
}

void xor(int *end, int *start, int len){
	for(int i = 0; i < len * len; i++) end[i] = start[i] ^ end[i];
}

void merge_buffer(int *end, int *start, int len){
	for(int i = 0; i < len * len; i++) end[i] = end[i] * 2 + start[i];
}

void split_buffer(int *bufferA, int *bufferB, int *merged, int len){
	for(int i = 0; i < len * len; i++){
		int x, y;
		x = merged[i] / 2;
		y = merged[i] % 2;
		bufferA[i] = x;
		bufferB[i] = y;
	}
}

void copy_buffer(int *temp, int *buffer, int len){
	for(int i = 0; i < len * len; i++) temp[i] = buffer[i];
}

int check_type(int *buffer, int i){
	if((buffer[i] || buffer[i + 1]) == 0) return 0;
	return 1;
}

int rm_msb(int n){
	int i;
	for(i = 1; n / i; i *= 2);
	return n ^ i / 2;
}

int count_bit(int n){
	int i;
  if (n == 0) return 1;
  for(i = 0; n != 0; i++){
    n >>= 1;
  }
  return i;
}

int encode_rle(char* buffer, int n){
	n++;
	int v = rm_msb(n);
	int l = n - v - 2;
	int k = count_bit(n) - 1;
	int_binstr(buffer, l, k);
	int_binstr(buffer + k, v, k);

	return k * 2;
}
