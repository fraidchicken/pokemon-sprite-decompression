#include <stdio.h>
#include <stdlib.h>
#include "header.h"

void load_buffer_2bpp(FILE *f, int *buffer, int len){
	for(int i = 0; i < len; i++){
		for(int j = 0; j < len / 2; j++){
			buffer[i * 2 + j * len * 2] = fgetc(f) - 48;
			buffer[i * 2 + j * len * 2 + 1] = fgetc(f) - 48;
		}
		fgetc(f); //'\n'
	}
}

void delta_encode(int *buffer, int len){
	int flag;
	for(int i = 0; i < len; i++){
		flag = 0;
		for(int j = 0; j < len / 2; j++){
			for(int k = 0; k < 2; k++){
				if(buffer[i * 2 + j * len * 2 + k] == 1 && flag == 0){
					flag = 1;
				}else if(buffer[i * 2 + j * len * 2 + k] == 1 && flag == 1){
					buffer[i * 2 + j * len * 2 + k] = 0;
				}else if(buffer[i * 2 + j * len * 2 + k] == 0 && flag == 1){
					buffer[i * 2 + j * len * 2 + k] = 1;
					flag = 0;
				}
			}
		}
	}
}

void init_buffer(char* buffer, int width, int height){
	int_binstr(buffer, width, 4);
	int_binstr(buffer + 4, height, 4);
	buffer[8] = '0'; //primary buffer - A
}

int encode(char *out, int *buffer, int len){
	int offset = 0; //out
	int counter = 0; //buffer
	int rle_counter = 0;
	int packet_type = 1;

	if(buffer[0] == 0 && buffer[1] == 0) packet_type = 0;
	out[offset++] = packet_type + 48;

	while(counter < len * len){
		if(packet_type == 0){
			rle_counter = 0;
			while(counter < len * len && check_type(buffer, counter) == 0){
				counter += 2;
				rle_counter++;
			}
			offset += encode_rle(out + offset, rle_counter);
			packet_type = 1;
		}else{
			while(counter < len * len && check_type(buffer, counter) == 1){
				out[offset++] = buffer[counter++] + 48;
				out[offset++] = buffer[counter++] + 48;
			}
			out[offset++] = '0';
			out[offset++] = '0';

			packet_type = 0;
		}
	}
	return offset;
}

void encode_mode1(char *buffer, int *bufferA, int *bufferB, int len){
	int *tempA = malloc(sizeof(int) * len * len);
	int *tempB = malloc(sizeof(int) * len * len);

	copy_buffer(tempA, bufferA, len);
	copy_buffer(tempB, bufferB, len);

	delta_encode(tempB, len);
	delta_encode(tempA, len);

	print_buffer_1bpp(tempA, len);
	print_buffer_1bpp(tempB, len);

	int offset = 0;
	offset += encode(buffer + offset, tempA, len);
	buffer[offset++] = '0';
	offset += encode(buffer + offset, tempB, len);
	buffer[offset++] = '\0';
}

void encode_mode2(char *buffer, int *bufferA, int *bufferB, int len){
	int *tempA = malloc(sizeof(int) * len * len);
	int *tempB = malloc(sizeof(int) * len * len);

	copy_buffer(tempA, bufferA, len);
	copy_buffer(tempB, bufferB, len);

	xor(tempB, tempA, len);
	delta_encode(tempA, len);

	print_buffer_1bpp(tempA, len);
	print_buffer_1bpp(tempB, len);

	int offset = 0;
	offset += encode(buffer + offset, tempA, len);
	buffer[offset++] = '1';
	buffer[offset++] = '0';
	offset += encode(buffer + offset, tempB, len);
	buffer[offset++] = '\0';
}

void encode_mode3(char *buffer, int *bufferA, int *bufferB, int len){
	int *tempA = malloc(sizeof(int) * len * len);
	int *tempB = malloc(sizeof(int) * len * len);

	copy_buffer(tempA, bufferA, len);
	copy_buffer(tempB, bufferB, len);

	xor(tempB, tempA, len);
	delta_encode(tempB, len);
	delta_encode(tempA, len);

	print_buffer_1bpp(tempA, len);
	print_buffer_1bpp(tempB, len);

	int offset = 0;
	offset += encode(buffer + offset, tempA, len);
	buffer[offset++] = '1';
	buffer[offset++] = '1';
	offset += encode(buffer + offset, tempB, len);
	buffer[offset++] = '\0';
}

int main(){
	FILE *f = fopen("decompressed.txt", "r");
	int width, height;

	fscanf(f, "%d %d\n", &width, &height);
	int len = width * 8;
	int size = len * len;
	int *bufferA = malloc(sizeof(int) * size);
	int *bufferB = malloc(sizeof(int) * size);

	load_buffer_2bpp(f, bufferA, len);
	print_buffer_2bpp(bufferA, len);
	split_buffer(bufferA, bufferB, bufferA, len);

	print_buffer_1bpp(bufferA, len);
	print_buffer_1bpp(bufferB, len);

	char *encoded_mode1 = malloc(sizeof(char) * size * 2);
	char *encoded_mode2 = malloc(sizeof(char) * size * 2);
	char *encoded_mode3 = malloc(sizeof(char) * size * 2);

	init_buffer(encoded_mode1, width, height);
	init_buffer(encoded_mode2, width, height);
	init_buffer(encoded_mode3, width, height);

	encode_mode1(encoded_mode1 + 9, bufferA, bufferB, len);
	encode_mode2(encoded_mode2 + 9, bufferA, bufferB, len);
	encode_mode3(encoded_mode3 + 9, bufferA, bufferB, len);

	printf("encoded_mode1: %s\n", encoded_mode1);
	printf("encoded_mode2: %s\n", encoded_mode2);
	printf("encoded_mode3: %s\n", encoded_mode3);
	return 0;
}
