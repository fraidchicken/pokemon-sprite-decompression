From a text file with the compressed sprite data to a decompressed 2bpp sprite

Thanks [Retro Game Mechanics Explained](https://www.youtube.com/channel/UCwRqWnW5ZkVaP_lZF7caZ-g)
 for the idea and the awesome 4 and a half hours livestream

Note: the data in `sprite.txt` is the same from the live

Extra Notes:
- Better data structure (struct {buffer, len})
- Better memory managment (1 bit != int)
- Load from binary stream
