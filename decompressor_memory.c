#include <stdio.h>
#include <stdlib.h>

int offset, offset_bit;

int mem_get(int *memory_buff){
	int c = memory_buff[offset] >> offset_bit & 1;
	//printf("memory_buff %d\n", memory_buff[offset]);
	//printf("mem_get: %d\t%d\t%d\n", c, offset, offset_bit);
	offset_bit--;
	if(offset_bit < 0){
		offset++;
		offset_bit = 7;
	}
	return c;
}

void mem_gets(int *buff, int n, int *memory_buff){
	//printf("mem_gets n: %d\n", n);
	for(int i = 0; i < n; i++) buff[i] = mem_get(memory_buff);
	//for(int i = 0; i < n; i++) printf("%d ", buff[i]);
	//printf("\n");
}

int binstr_int(char *buf){
	int n = 0;
	for(int i = 0; buf[i] != '\0'; i++){
		n = n * 2 + (buf[i] == '1' ? 1 : 0);
	}

	return n;
}

int bin_int(int *buff, int len){
	int n = 0;
	for(int i = 0; i < len; i++){
		//printf("bin_int: %d\n", buff[i]);
		n = n * 2 + buff[i];
	}
	return n;
}

void print_buffer_1bpp(int *buffer, int len){
	char list_1bpp[] = " #";
	for(int i = 0; i < len; i++){
		for(int j = 0; j < len / 2; j++){
			printf("%c", list_1bpp[buffer[i * 2 + j * len * 2]]);
			printf("%c", list_1bpp[buffer[i * 2 + j * len * 2 + 1]]);
		}
		printf("\n");
	}
}

void print_buffer_2bpp(int *buffer, int len){
	char list_2bpp[] = " .*#";
	for(int i = 0; i < len; i++){
		for(int j = 0; j < len / 2; j++){
			printf("%c", list_2bpp[buffer[i * 2 + j * len * 2]]);
			printf("%c", list_2bpp[buffer[i * 2 + j * len * 2 + 1]]);
		}
		printf("\n");
	}
}

void mem_getRLE(int *buf, int *mem_buff){
	int count = 0, n, n1 = 0, n2 = 0;
	do{
		n = mem_get(mem_buff);
		n1 = n1 * 2 + n;
		count++;
	}while(n != 0);

	int i;
	for(i = 0; i < count; i++){
		n = mem_get(mem_buff);
		n2 = n2 * 2 + n;
	}

	int sum = n1 + n2 + 1;

	//printf("mem_getRLE: %d\n", sum);
	//getchar();

	for(i = 0; i < sum * 2; i++) buf[i] = 0;
	buf[i] = 2;
}

void mem_getData(int *buf, int *mem_buff){
	int i = 0, flag = 1;
	int c1, c2;

	while(flag){
		c1 = mem_get(mem_buff);
		c2 = mem_get(mem_buff);

		//printf("mem_getData: %d %d\n", c1, c2);
		//getchar();

		if(c1 == 0 && c2 == 0){
			buf[i] = 2;
			flag = 0;
		}else{
			buf[i] = c1;
			i++;
			buf[i] = c2;
			i++;
		}
	}
}

void fill_buffer(int *buffer_end, int *mem_buff, int size, int packet_type){
	int counter = 0;
	int buffer[1024];

	while(counter < size){
		//printf("packet_type: %d\n", packet_type);
		switch(packet_type){
			case 0:
				mem_getRLE(buffer, mem_buff);
				break;
			case 1:
				mem_getData(buffer, mem_buff);
				break;
		}

		for(int i = 0; buffer[i] != 2 && counter < size; counter++, i++){
			buffer_end[counter] = buffer[i];
		}

		//printf("Counter: %d\tSize: %d\n", counter, size);
		//printf("Packet_type: %d\t", packet_type);
		//for(int i = 0; buffer[i] != 2; i++) printf("%d", buffer[i]);
		//printf("\n");

		packet_type = packet_type == 0 ? 1 : 0;
	}
}

void delta_decode(int *buffer, int len){
	int flag = 0;

	for(int i = 0; i < len; i++){
		flag = 0;
		for(int j = 0; j < len / 2; j++){
			for(int k = 0; k < 2; k++){ //sorry
				if(buffer[i * 2 + j * 2 * len + k] == 1 && flag == 0){
					flag = 1;
				}else if(buffer[i * 2 + j * 2 * len + k] == 1 && flag == 1){
					buffer[i * 2 + j * 2 * len + k] = 0;
					flag = 0;
				}else if(buffer[i * 2 + j * 2 * len + k] == 0 && flag == 1){
					buffer[i * 2 + j * 2 * len + k] = 1;
				}
			}
		}
	}
}

void xor(int *end, int *start, int len){
	for(int i = 0; i < len * len; i++) end[i] = start[i] ^ end[i];
}

void merge_buffer(int *end, int *start, int len){
	for(int i = 0; i < len * len; i++) end[i] = end[i] * 2 + start[i];
}

int main(){
	FILE *f = fopen("Pokemon Red.gb", "rb");
	int mem_buffer[65536];
	for(int i = 0; i < 65536; i++){
		mem_buffer[i] = fgetc(f);
		if(mem_buffer[i] == -1) printf("fgetc: -1\n");
		//printf("fgetc: %d\n", mem_buffer[i]);
	}
	offset = 16383;
	offset_bit = 7;

	int buffer[8], start, primary;
	int size = 1;

	mem_gets(buffer, 4, mem_buffer);	//width
	size *= bin_int(buffer, 4) * 8;
	int len = size;
	mem_gets(buffer, 4, mem_buffer);	//height
	size *= bin_int(buffer, 4) * 8;

	int *bufferA = malloc(sizeof(int) * size);
	int *bufferB = malloc(sizeof(int) * size);

	primary = mem_get(mem_buffer);		//primary buffer
	start = mem_get(mem_buffer);		//0 - RLE, 1 - Data

	if(primary == 0){
		int* temp = bufferA;
		bufferA = bufferB;
		bufferB = temp;
	}

	fill_buffer(bufferA, mem_buffer, size, start);

	//encoding
	int encoding = mem_get(mem_buffer) == 1 ? mem_get(mem_buffer) + 1 : 0;

	start = mem_get(mem_buffer);

	fill_buffer(bufferB, mem_buffer, size, start);

	delta_decode(bufferA, len);

	switch(encoding){
		case 0:
			printf("Mode 1\n");
			delta_decode(bufferB, len);
			break;
		case 1:
			printf("Mode 2\n");
			xor(bufferB, bufferA, len);
			break;
		case 2:
			printf("Mode 3\n");
			delta_decode(bufferB, len);
			xor(bufferB, bufferA, len);
			break;
	}

	print_buffer_1bpp(bufferA, len);
	print_buffer_1bpp(bufferB, len);

	merge_buffer(bufferA, bufferB, len);
	print_buffer_2bpp(bufferA, len);
	return 0;
}
